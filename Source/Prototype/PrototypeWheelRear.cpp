// Copyright Epic Games, Inc. All Rights Reserved.

#include "PrototypeWheelRear.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UPrototypeWheelRear::UPrototypeWheelRear()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = true;
	SteerAngle = 0.f;
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS
