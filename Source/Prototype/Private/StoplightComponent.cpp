// Fill out your copyright notice in the Description page of Project Settings.


#include "StoplightComponent.h"

#include "VehicleAIController.h"
#include "Prototype/PrototypePawn.h"

// Sets default values for this component's properties
UStoplightComponent::UStoplightComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	OnComponentBeginOverlap.AddDynamic(this, &UStoplightComponent::OnPlayerEnter);
	OnComponentEndOverlap.AddDynamic(this, &UStoplightComponent::OnPlayerExit);
}


// Called when the game starts
void UStoplightComponent::BeginPlay()
{
	Super::BeginPlay();
	
	// ...
	crossroadManager->change.AddDynamic(this, &UStoplightComponent::UpdateStatus);
}

void UStoplightComponent::UpdateStatus(int cross)
{
	UE_LOG(LogTemp, Warning, TEXT("Stoplight Delegate %s"), *(GetOwner()->GetName()))
	status = crossroadN == cross ? GREEN : RED;

	if(status == GREEN)
	{
		UE_LOG(LogTemp, Warning, TEXT("Crossroad now green"))
		for(AActor* other : vehiclesInTrigger)
		{
			APrototypePawn* vehicle = Cast<APrototypePawn>(other);
			//No checking for null since array vehiclesInTrigger contains ONLY vehicles

			if(vehicle->aiController != nullptr)
			{
				AVehicleAIController* vehicleAI = Cast<AVehicleAIController>(vehicle->aiController);
				vehicleAI->SetVelocityMultiplier(1);
				UE_LOG(LogTemp, Error, TEXT("Vehicles on crossroad can now move."))
			}
		}
	}
}

void UStoplightComponent::OnPlayerEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APrototypePawn* vehicle = Cast<APrototypePawn>(OtherActor);
	if(vehicle != nullptr)
	{
		vehiclesInTrigger.Add(OtherActor);
			UE_LOG(LogTemp, Error, TEXT("Spotlight::OnPlayerEnter"))

		if(status == RED)
		{
			if(vehicle->aiController != nullptr)
			{
				AVehicleAIController* vehicleAI = Cast<AVehicleAIController>(vehicle->aiController);
				vehicleAI->SetVelocityMultiplier(0);
			}
		}
	}
}

void UStoplightComponent::OnPlayerExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APrototypePawn* vehicle = Cast<APrototypePawn>(OtherActor);
	if(vehicle != nullptr )
	{
		if(vehiclesInTrigger.Contains(vehicle))
		{
			vehiclesInTrigger.Remove(OtherActor);
			UE_LOG(LogTemp, Error, TEXT("Spotlight::OnPlayerExit"))
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Unexpected behavior"))
		}
		if(vehicle->aiController != nullptr)
		{
			AVehicleAIController* vehicleAI = Cast<AVehicleAIController>(vehicle->aiController);
			vehicleAI->SetVelocityMultiplier(1);
		}
	}
}