// Fill out your copyright notice in the Description page of Project Settings.

#include "VehicleAIController.h"
#include "PxMathUtils.h"

AVehicleAIController::AVehicleAIController()
{
	//Initialization
	PrimaryActorTick.bCanEverTick = true;
	maxVelocity = 100000;
}

void AVehicleAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if(GetPawn() == nullptr) return;
	
	if (vehicleActor != nullptr)
	{
		FVector relativePosition = targetWaypoint->GetActorLocation() - vehicleActor->GetActorLocation();

		velocityVec = vehicleActor->GetVelocity();
		vehicleVel = velocityVec.Size();
		targetVel = 60000;
		ManageVicinities();
		
		fwd = vehicleActor->GetActorForwardVector();
		
		float aimAtAngle = GetAngleBetween(fwd, relativePosition);
		//Moving Vertically
		if (FMath::Abs(relativePosition.Y) > FMath::Abs(relativePosition.X))
		{
			if (relativePosition.Y < 0)
			{
				//Moving up
				if (aimAtAngle > 10)
				{
					if(velocityVec.X > 0)
						steerDir = -1;
					else
						steerDir = 1;
				}
				else
				{
					steerDir = relativePosition.X > 0 ? 1 : -1;
				}
			}
			else
			{
				//Moving down
				if (aimAtAngle > 10)
				{
					if (velocityVec.X > 0)
						steerDir = 1;
					else
						steerDir = -1;
				}
				else 
				{
					steerDir = relativePosition.X > 0 ? -1 : 1;
				}
				//UE_LOG(LogTemp, Warning, TEXT("Moving downwards."));
			}
		}
		else
		{
			//Moving sideways
			if (relativePosition.X > 0)
			{
				//Moving right

				if (aimAtAngle > 10)
				{
					if (velocityVec.Y < 0)
						steerDir = 1;
					else
						steerDir = -1;
				}
				else
				{
					steerDir = relativePosition.Y < 0 ? -1 : 1;
				}
				//UE_LOG(LogTemp, Warning, TEXT("Moving to the right."));
			}
			else
			{
				//Moving left
				if (aimAtAngle > 10)
				{
					if (velocityVec.Y < 0)
						steerDir = -1;
					else
						steerDir = 1;
				}
				else
				{
					steerDir = relativePosition.Y < 0 ? 1 : -1;
				}
			}
		}

		if(velocityMultiplier != 0) velocityMultiplier = 1;
		ManageBadSituations(DeltaSeconds);
		
		//Apply steer
		float steerAmount = (aimAtAngle / 45 * steerDir + steeringOffset) * overrideDir;
		vehicleActor->MoveRight(steerAmount);
		steeringOffset = 0;
		
		//Apply acceleration
		vehicleActor->MoveForward(FMath::Abs(vehicleVel - targetVel) / maxVelocity * velocityMultiplier);
	}
	else
	{
		vehicleActor = Cast<APrototypePawn>(GetPawn());
		vehicleActor->aiController = this;
		if (targetWaypoint == nullptr && vehicleActor != nullptr)
		{
			SetWaypoint(vehicleActor->firstWaypoint);
		}

		UE_LOG(LogTemp, Warning, TEXT("Setting Pawn..."))
	}
}

void AVehicleAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
}

void AVehicleAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AVehicleAIController::SetVelocityMultiplier(float multiplier)
{
	velocityMultiplier = multiplier;
}

float AVehicleAIController::GetAngleBetween(FVector a, FVector b)
{
	FVector2D a1 = FVector2D(a);
	FVector2D b1 = FVector2D(b);
	a1.Normalize();
	b1.Normalize();

	float aimAtAngle = FMath::RadiansToDegrees(acosf(FVector2D::DotProduct(a1, b1)));
	return aimAtAngle;
}

void AVehicleAIController::SetWaypoint(AActor* waypoint)
{
	targetWaypoint = waypoint;
}

void AVehicleAIController::ManageVicinities()
{
	// Set what actors to seek out from it's collision channel
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Vehicle));

	// Ignore any specific actors
	TArray<AActor*> ignoreActors;
	// Ignore self or remove this line to not ignore any
	ignoreActors.Init(this, 1);
	ignoreActors.Init(vehicleActor, 1);

	// Array of actors that are inside the radius of the sphere
	TArray<AActor*> outActors;

	// Total radius of the sphere
	float radius = 3000;
	// Sphere's spawn loccation within the world
	FVector sphereSpawnLocation = vehicleActor->GetActorLocation();
	// Class that the sphere should hit against and include in the outActors array (Can be null)
	// UClass* seekClass = AEnemyBase::StaticClass(); // NULL;
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), sphereSpawnLocation, radius, traceObjectTypes, nullptr, ignoreActors, outActors);

	// Optional: Use to have a visual representation of the SphereOverlapActors
	UKismetSystemLibrary::DrawDebugSphere(GetWorld(), vehicleActor->GetActorLocation(), radius, 12, FColor::Red, 0, 1);

	// Finally iterate over the outActor array
	for (AActor* overlappedActor : outActors) 
	{		
		float angleBetweenSelfAndOther = GetAngleBetween(vehicleActor->GetActorForwardVector(), overlappedActor->GetActorLocation() - vehicleActor->GetActorLocation());
		float relativeDist = FVector2D(vehicleActor->GetActorLocation() - overlappedActor->GetActorLocation()).Size();


		//1100 is the amount of distance in units required for the vehicle to hit the breaks and reach 0 velocity
		//when he's going at 5000 velocity forwards.
		//The formula just calculates the distance threshold in addition to 1100 to reach 0 velocity on time.
		if (angleBetweenSelfAndOther < 22.5f && relativeDist < 1100 + (1100 * vehicleVel / 5000))
		{
			targetVel = overlappedActor->GetVelocity().Size2D();
			//UE_LOG(LogTemp, Warning, TEXT("Actor %s is in the way of the vehicle."), *(overlappedActor->GetName()))
		}
		
		//Steer out of the way if in danger
		APrototypePawn* playerVehicle = Cast<APrototypePawn>(overlappedActor);
		if(playerVehicle != nullptr)
		{
			if(playerVehicle->driver != nullptr)
			{
				const float angleBetweenSelfAndVehicleDir = 200 - GetAngleBetween(vehicleActor->GetActorForwardVector(), playerVehicle->GetActorForwardVector());
				const float otherVehicleVel = FMath::Abs(playerVehicle->GetVelocity().Size());
				
				if(angleBetweenSelfAndVehicleDir < 30)
                {
					//Steer left, out of the way
					steeringOffset = (otherVehicleVel / maxVelocity) * -50;
					UE_LOG(LogTemp, Warning, TEXT("Moving out of the way"))
					//steeringOffset = -10;
				}
			}
		}
	}
}

void AVehicleAIController::ManageBadSituations(float DeltaSeconds)
{
	float Reach = 600;
	FVector LineTraceStart = vehicleActor->GetActorLocation() + FVector::UpVector * 50;
	FVector LineTraceEnd = LineTraceStart + fwd * Reach;

	UKismetSystemLibrary::DrawDebugLine(GetWorld(), vehicleActor->GetActorLocation(), LineTraceEnd, FLinearColor::Green);

	// Check if the vehicle is in a bad state and reverse out of it
	FHitResult Hit;
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByChannel(
		OUT Hit,
		vehicleActor->GetActorLocation(),
		LineTraceEnd,
		ECC_Visibility,
		TraceParams
	);
	bool stuckOnLeft = false;
	if(Hit.GetActor() != nullptr && vehicleVel < 500)
	{
		APawn* otherVehicle = Cast<APawn>(Hit.GetActor());
		if(otherVehicle == nullptr)
		{
			//if not vehicle then
			idleT += DeltaSeconds;
			if(idleT >= 1.0f)
			{
				stuckOnLeft = steerDir < 0;
				overrideDir = stuckOnLeft ? -steerDir : steerDir;
				velocityMultiplier = -1;
				UE_LOG(LogTemp, Warning, TEXT("REVERSING"))
			}
		}
	}
	else if(idleT > 0)
	{
		idleT = 0;
		overrideDir = 1;
	}
}
