// Fill out your copyright notice in the Description page of Project Settings.


#include "CrossroadManager.h"

// Sets default values
ACrossroadManager::ACrossroadManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACrossroadManager::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACrossroadManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	timer += DeltaTime / lightDuration;
	if(timer >= 1)
	{
		currIntersection++;
		if(currIntersection > intersectionCount)
			currIntersection = 1;
		change.Broadcast(currIntersection);
		UE_LOG(LogTemp, Warning, TEXT("Intersection Update"))
		timer = 0;
	}
}

