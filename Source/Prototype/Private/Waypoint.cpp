// Fill out your copyright notice in the Description page of Project Settings.


#include "Waypoint.h"
#include "..\PrototypePawn.h"
#include "VehicleAIController.h"
#include "..\Public\Waypoint.h"

// Sets default values
AWaypoint::AWaypoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Root);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	BoxComponent->SetupAttachment(GetRootComponent());
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AWaypoint::OnPlayerEnter);
	Root->SetWorldScale3D(FVector(7, 7, 5));
}

// Called when the game starts or when spawned
void AWaypoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWaypoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWaypoint::OnPlayerEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr)
	{
		APrototypePawn* pawn = Cast<APrototypePawn>(OtherActor);
		if (pawn != nullptr)
		{
			if (pawn->driver)
			{
				//Then player is controlling
			}
			else
			{
				//Then AI might be controlling
				AVehicleAIController* ai = Cast<AVehicleAIController>(pawn -> GetController());
				if (ai != nullptr)
				{
					if(ai->targetWaypoint == this)
						ai->SetWaypoint(possibleWaypoints[rand() % possibleWaypoints.Num()]);
				}
				else
				{
					//UE_LOG(LogTemp, Error, TEXT("Cast to obtain vehicle ai failed."));
				}
			}
		}
	}
}

