// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "PrototypeWheelRear.generated.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UCLASS()
class UPrototypeWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UPrototypeWheelRear();
};

PRAGMA_ENABLE_DEPRECATION_WARNINGS

