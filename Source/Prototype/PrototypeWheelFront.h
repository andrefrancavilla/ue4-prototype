// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "PrototypeWheelFront.generated.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UCLASS()
class UPrototypeWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UPrototypeWheelFront();
};

PRAGMA_ENABLE_DEPRECATION_WARNINGS


