// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "PrototypeHud.generated.h"


UCLASS(config = Game)
class APrototypeHud : public AHUD
{
	GENERATED_BODY()

public:
	APrototypeHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface
};
