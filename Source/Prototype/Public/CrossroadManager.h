// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CrossroadManager.generated.h"

UENUM()
enum StoplightStatus { RED, GREEN };

UCLASS()
class PROTOTYPE_API ACrossroadManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACrossroadManager();
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStoplightChange, int, cross);
	FStoplightChange change;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Design)
	int intersectionCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Design)
	float lightDuration = 2;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	int currIntersection = 1;
	float timer;
};
