// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CrossroadManager.h"
#include "Components/BoxComponent.h"

#include "StoplightComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROTOTYPE_API UStoplightComponent : public UBoxComponent
{
	GENERATED_BODY()

	
public:	
	// Sets default values for this component's properties
	UStoplightComponent();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Design)
	ACrossroadManager* crossroadManager;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Design)
	int crossroadN;
	StoplightStatus status;

	UFUNCTION()
	void UpdateStatus(int cross);
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void OnPlayerEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
	void OnPlayerExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	TArray<AActor*> vehiclesInTrigger;

		
};
