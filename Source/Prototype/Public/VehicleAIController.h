// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h" 
#include "AIController.h"
#include "../Classes/Kismet/KismetSystemLibrary.h"
#include "../TP_ThirdPerson/TP_ThirdPersonCharacter.h"
#include "VehicleAIController.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPE_API AVehicleAIController : public AAIController
{
	GENERATED_BODY()
public:
	AVehicleAIController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void OnPossess(APawn* InPawn) override;
	void SetWaypoint(AActor* waypoint);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Design)
		APrototypePawn* vehicleActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Design)
		AActor* targetWaypoint;

	void SetVelocityMultiplier(float multiplier);
	
	float maxVelocity;
	float velocityMultiplier = 1;
	static float GetAngleBetween(FVector a, FVector b);
private:
	void ManageVicinities();
	void ManageBadSituations(float DeltaSeconds);
	
	int steerDir;
	int overrideDir = 1;
	float targetVel;
	float vehicleVel;
	float steeringOffset = 0;
	FVector velocityVec;
	TArray<AActor*> nearbyActors;

	float idleT;
	FVector fwd;
};
