// Copyright Epic Games, Inc. All Rights Reserved.

#include "PrototypeGameMode.h"
#include "PrototypePawn.h"
#include "PrototypeHud.h"

APrototypeGameMode::APrototypeGameMode()
{
	DefaultPawnClass = APrototypePawn::StaticClass();
	HUDClass = APrototypeHud::StaticClass();
}
